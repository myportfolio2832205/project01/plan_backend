const express = require('express');
const router = express.Router();

const productController = require('../controllers/productController.js');
const auth = require('../auth.js');

// ----------------------------- //
// ----- Table of Contents ----- //
// ----------------------------- //
// [PD] Product Details
// ----- [PD/C] Create
// ----- [PD/RAA] Retrieve All Active
// ----- [PD/RAABH] Retrieve All Active By Handle
// ----- [PD/RF] Retrieve Featured
// ----- [PD/RA] Retrieve All
// ----- [PD/R] Retrieve
// ----- [PD/RBH] Retrieve By Handle
// ----- [PD/U] Update
// [PA] Product Archive
// ----- [PA/U] Unarchive
// ----- [PA/A] Archive
// ----------------------------- //
// ----------------------------- //

// -------------------------------- //
// ----- [PD] Product Details ----- //
// -------------------------------- //

// ----- [PD/C] Create ----- //
router.post('/create', auth.verify, (req, res) => {
    const data = {
        body: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    };

    productController.create(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [PD/RAA] Retrieve All Active ----- //
router.get('/active', (req, res) => {
    productController.retrieveAllActive().then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [PD/RAABH] Retrieve All Active By Handle ----- //
router.get('/active/by-handle/:collectionsHandle', (req, res) => {
    const data = {
        params: req.params,
    };

    productController.retrieveAllActiveByHandle(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [PD/RF] Retrieve Featured ----- //
router.get('/featured', (req, res) => {
    productController.retrieveFeatured().then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [PD/RA] Retrieve All ----- //
router.get('/', (req, res) => {
    productController.retrieveAll().then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [PD/R] Retrieve ----- //
router.get('/:productId', (req, res) => {
    const data = {
        params: req.params,
    };

    productController.retrieve(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [PD/RBH] Retrieve By Handle ----- //
router.get('/by-handle/:productHandle', (req, res) => {
    const data = {
        params: req.params,
    };

    productController.retrieveByHandle(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [PD/U] Update ----- //
router.patch('/:productId/update', auth.verify, (req, res) => {
    const data = {
        body: req.body,
        params: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    };

    productController.update(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// -------------------------------- //
// ----- [PA] Product Archive ----- //
// -------------------------------- //

// ----- [PA/U] Unarchive ----- //
router.patch('/:productId/unarchive', auth.verify, (req, res) => {
    const data = {
        body: req.body,
        params: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    };

    productController.setActive(data, true).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [PA/A] Archive ----- //
router.patch('/:productId/archive', auth.verify, (req, res) => {
    const data = {
        body: req.body,
        params: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    };

    productController.setActive(data, false).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

module.exports = router;
