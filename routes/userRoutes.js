const express = require('express');
const router = express.Router();

const userController = require('../controllers/userController.js');
const auth = require('../auth.js');

// ----------------------------- //
// ----- Table of Contents ----- //
// ----------------------------- //
// [UA] User Authentication
// ----- [UA/R] Register
// ----- [UA/L] Login
// [UD] User Details
// ----- [UD/RAU] Retrieve All Users
// ----- [UD/RU] Retrieve User
// ----- [UD/CP] Change Password
// ----- [UD/SA] Set to Admin
// ----- [UD/SU] Set to User
// [C] Cart
// ----- [C/CR] Retrieve Cart
// ----- [C/AC] Add to Cart
// ----- [C/UCI] Update Cart Item
// ----- [C/EC] Empty Cart
// ----------------------------- //
// ----------------------------- //

// ------------------------------------ //
// ----- [UA] User Authentication ----- //
// ------------------------------------ //

// ----- [UA/R] Register ----- //
router.post('/register', (req, res) => {
    const data = {
        body: req.body,
    };

    userController.register(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [UA/L] Login ----- //
router.post('/login', (req, res) => {
    const data = {
        body: req.body,
    };

    userController.login(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// -------------------------- //
// ----- [UD] User Data ----- //
// -------------------------- //

// ----- [UD/RAU] Retrieve All Users ----- //
router.get('/', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    userController.retrieveAll(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [UD/RU] Retrieve User ----- //
router.get('/:userId', auth.verify, (req, res) => {
    const data = {
        params: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    userController.retrieve(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [UD/CP] Change Password ----- //
router.patch('/:userId/password', auth.verify, (req, res) => {
    const data = {
        params: req.params,
        body: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    userController.changePassword(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [UD/SA] Set to Admin ----- //
router.patch('/:userId/status/admin', auth.verify, (req, res) => {
    const data = {
        body: req.body,
        params: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    userController.setAdmin(data, true).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [UD/SU] Set to User ----- //
router.patch('/:userId/status/user', auth.verify, (req, res) => {
    const data = {
        body: req.body,
        params: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    userController.setAdmin(data, false).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// -------------------- //
// ----- [C] Cart ----- //
// -------------------- //

// ----- [C/CR] Retrieve Cart ----- //
router.get('/:userId/cart', auth.verify, (req, res) => {
    const data = {
        params: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    userController.retrieveCart(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [C/AC] Add to Cart ----- //
router.patch('/:userId/cart/add', auth.verify, (req, res) => {
    const data = {
        params: req.params,
        body: req.body,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    userController.addToCart(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [C/UCI] Update Cart Item ----- //
router.patch('/:userId/cart/update', auth.verify, (req, res) => {
    const data = {
        params: req.params,
        body: req.body,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    userController.updateCartItem(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [C/EC] Empty Cart ----- //
router.patch('/:userId/cart/empty', auth.verify, (req, res) => {
    const data = {
        params: req.params,
        body: req.body,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    userController.emptyCart(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

module.exports = router;
