const express = require('express');
const router = express.Router();

const orderController = require('../controllers/orderController.js');
const auth = require('../auth.js');

// ----------------------------- //
// ----- Table of Contents ----- //
// ----------------------------- //
// [O] Orders
// ----- [O/CO] Create Order
// ----- [O/RA] Retrieve All
// ----- [O/RSO] Retrieve Single Order
// ----- [O/RUO] Retrieve User Orders
// ----------------------------- //
// ----------------------------- //

// ---------------------- //
// ----- [O] Orders ----- //
// ---------------------- //

// ----- [O/CO] Create Order ----- //
router.post('/create', auth.verify, (req, res) => {
    const data = {
        body: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    orderController.create(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [O/RA] Retrieve All ----- //
router.get('/', auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
    };

    orderController.retrieveAll(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [O/RSO] Retrieve Single Order ----- //
router.get('/:orderId', (req, res) => {
    const data = {
        params: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    orderController.retrieve(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

// ----- [O/RUO] Retrieve User Orders ----- //
router.get('/user/:userId', auth.verify, (req, res) => {
    const data = {
        params: req.params,
        isAdmin: auth.decode(req.headers.authorization).isAdmin,
        accessorId: auth.decode(req.headers.authorization).id,
    };

    orderController.retrieveUserOrders(data).then((result) => {
        res.status(result.status).send(result.data);
    }).catch((result) => {
        res.status(400).send({ errors: result.errors });
    });
});

module.exports = router;
