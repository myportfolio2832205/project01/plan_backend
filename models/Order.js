const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, 'User Id is required'],
    },
    products: [
        {
            name: {
                type: String,
                required: [true, 'Name is required'],
            },
            price: {
                type: Number,
                required: [true, 'Price is required'],
            },
            quantity: {
                type: Number,
                required: [true, 'Quantity is required'],
            },
            image: {
                type: String,
                required: [true, 'Name is required'],
            },
        },
    ],
    totalPrice: {
        type: Number,
        default: 0,
    },
    deliveryAddress: {
        type: String,
        required: [true, 'Delivery Address is required'],
    },
    isDelivered: {
        type: Boolean,
        default: false,
    },
    paymentMethod: {
        type: String,
        default: 'COD',
    },
    purchasedOn: {
        type: Date,
        default: new Date(),
    },
    createdOn: {
        type: Date,
        default: new Date(),
    },
    updatedOn: {
        type: Date,
        default: new Date(),
    },

});

module.exports = mongoose.model('Order', orderSchema);
