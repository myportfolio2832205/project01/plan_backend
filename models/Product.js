const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Name is required'],
    },
    handle: {
        type: String,
        required: [true, 'Handle is required'],
        validate: [
            /^([-a-z])*[^\s]\1*$/,
            'Use kebab case with lowercase letters only',
        ],
    },
    description: {
        type: String,
        required: [true, 'Description is required'],
    },
    price: {
        type: Number,
        min: 0,
        required: [true, 'Price is required'],
    },
    isActive: {
        type: Boolean,
        default: false,
    },
    rating: {
        type: Number,
        default: 0,
        min: 0,
        max: 5,
    },
    images: {
        type: String,
        required: false,
    },
    category: {
        type: String,
        required: false,
    },
    createdOn: {
        type: Date,
        default: new Date(),
    },
    updatedOn: {
        type: Date,
        default: new Date(),
    },

});

module.exports = mongoose.model('Product', productSchema);
