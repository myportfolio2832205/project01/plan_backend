const mongoose = require('mongoose');

const reviewSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, 'User Id is required'],
    },
    productId: {
        type: String,
        required: [true, 'Product Id is required'],
    },
    rating: {
        type: Number,
        min: 1,
        max: 5,
        required: [true, 'Rating is required'],
    },
    comment: {
        type: String,
        required: false,
    },
    createdOn: {
        type: Date,
        default: new Date(),
    },
    updatedOn: {
        type: Date,
        default: new Date(),
    },

});

module.exports = mongoose.model('Review', reviewSchema);
