/* eslint-disable no-await-in-loop */
/* eslint-disable no-underscore-dangle */
const bcrypt = require('bcrypt');

const User = require('../models/User.js');
const Product = require('../models/Product.js');
const Order = require('../models/Order.js');
const auth = require('../auth.js');

// ----------------------------- //
// ----- Table of Contents ----- //
// ----------------------------- //
// [O] Orders
// ----- [O/CO] Create Order
// ----- [O/RA] Retrieve All
// ----- [O/RSO] Retrieve Single Order
// ----- [O/RUO] Retrieve User Orders
// ----------------------------- //
// ----------------------------- //

// ---------------------- //
// ----- [O] Orders ----- //
// ---------------------- //

// ----- [O/CO] Create Order ----- //
module.exports.create = async (data) => {
    if (data.accessorId !== data.body.userId || data.isAdmin === true) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    let user = null;
    try {
        user = await User.findOne({ _id: data.body.userId }).then((result, error) => {
            if (error) {
                return null;
            }

            return {
                id: result._id,
                productsBought: result.productsBought,
                cart: result.cart,
            };
        });
    } catch {
        user = null;
    }

    if (user === null) {
        return {
            status: 404,
            data: {
                status: 404,
                message: `User with this id:${data.body.userId} cannot be found`,
            },
        };
    }

    if (user.cart.length < 1) {
        return {
            status: 404,
            data: {
                status: 404,
                message: 'User\'s cart is empty. Cannot create an order',
            },
        };
    }

    const productList = [];
    let totalAmount = 0;
    for (let index = 0; index < user.cart.length; index += 1) {
        const product = await Product.findOne({ _id: user.cart[index].productId }).then((result, error) => ({
            id: result._id,
            name: result.name,
            price: result.price,
            quantity: user.cart[index].quantity,
            images: result.images,
            isActive: result.isActive,
        }));

        if (product && product.isActive === true) {
            productList.push(product);
            totalAmount += (product.price * product.quantity);
        }
    }

    const finalProducts = productList.map((item) => {
        const image = (typeof item.images !== 'undefined')
            ? item.images.split(',')[0]
            : 'https://drive.google.com/uc?export=view&id=10SCYogmv3QU2rvqF7lG1GD6KD5AXyL7A';
        return {
            name: item.name,
            price: item.price,
            quantity: item.quantity,
            total: item.price * item.quantity,
            image,
        };
    });

    const newOrder = new Order({
        userId: user.id,
        products: finalProducts,
        totalPrice: totalAmount,
        deliveryAddress: data.body.deliveryAddress,
        paymentMethod: data.body.paymentMethod ?? 'Cash on Delivery',
    });

    return newOrder.save().then(async (result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Unable to create order at this time',
                },
            };
        }

        const newProducts = Array.from(productList.map((product) => String(product.id)));
        const updatedProductsBought = [
            ...Array.from(new Set(user.productsBought)),
            ...Array.from(new Set(newProducts)),
        ];
        const filteredProductsBought = Array.from(new Set(updatedProductsBought));

        await User.findByIdAndUpdate(user.id, {
            cart: [],
            productsBought: filteredProductsBought,
        });
        return {
            status: 201,
            data: {
                status: 201,
                message: `Order has been created for User with Id: ${user.id}`,
                order: result,
            },
        };
    });
};

// ----- [O/RA] Retrieve All ----- //
module.exports.retrieveAll = async (data) => {
    if (data.isAdmin === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    return Order.find({}).sort({ createdOn: 'desc' }).then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Failed to retrieve orders at this time',
                },
            };
        }

        return {
            status: 200,
            data: {
                status: 200,
                count: result.length,
                orders: result,
            },
        };
    });
};

// ----- [O/RSO] Retrieve Single Order ----- //
module.exports.retrieve = async (data) => Order.findOne({ _id: data.params.orderId }).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Unable to retrieve order at this time',
            },
        };
    }

    if (data.accessorId === result.userId || data.isAdmin === true) {
        return {
            status: 200,
            data: {
                status: 200,
                order: result,
            },
        };
    }

    return {
        status: 401,
        data: {
            status: 401,
            message: 'User is unauthorized to do this action',
        },
    };
}).catch((error) => ({
    status: 404,
    data: {
        status: 404,
        message: `Order with this id:${error.value} cannot be found`,
    },
}));

// ----- [O/RUO] Retrieve User Orders ----- //
module.exports.retrieveUserOrders = async (data) => {
    if (auth.isSelfOrAdmin(data) === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    let user = null;
    try {
        user = await User.findOne({ _id: data.params.userId }).then((result, error) => {
            if (error) {
                return null;
            }

            return {
                id: result._id,
                firstname: result.firstname,
                lastname: result.lastname,
            };
        });
    } catch {
        user = null;
    }

    if (user === null) {
        return {
            status: 404,
            data: {
                status: 404,
                message: `User with this id:${data.params.userId} cannot be found`,
            },
        };
    }

    return Order.find({ userId: data.params.userId }).sort({ createdOn: 'desc' }).then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Failed to retrieve orders at this time',
                },
            };
        }

        return {
            status: 200,
            data: {
                status: 200,
                customer: {
                    firstname: user.firstname,
                    lastname: user.lastname,
                },
                count: result.length,
                orders: result,
            },
        };
    });
};
