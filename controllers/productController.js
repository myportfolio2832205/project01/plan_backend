/* eslint-disable no-underscore-dangle */
const bcrypt = require('bcrypt');

const Product = require('../models/Product.js');
const auth = require('../auth.js');

// ----------------------------- //
// ----- Table of Contents ----- //
// ----------------------------- //
// [PD] Product Details
// ----- [PD/C] Create
// ----- [PD/RAA] Retrieve All Active
// ----- [PD/RAABH] Retrieve All Active By Handle
// ----- [PD/RF] Retrieve Featured
// ----- [PD/RA] Retrieve All
// ----- [PD/R] Retrieve
// ----- [PD/RBH] Retrieve By Handle
// ----- [PD/U] Update
// [PA] Product Archive
// ----- [PA/SA] Set Active
// ----------------------------- //
// ----------------------------- //

// -------------------------------- //
// ----- [PD] Product Details ----- //
// -------------------------------- //

// ----- [PD/C] Create ----- //
module.exports.create = async (data) => {
    if (data.isAdmin === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    const handleExist = await Product.find({ handle: data.body.handle }).then((result) => (result.length > 0));

    if (handleExist) {
        return {
            status: 405,
            data: {
                status: 405,
                message: 'Product with this handle already exist',
            },
        };
    }

    const newProduct = new Product({
        name: data.body.name,
        handle: data.body.handle,
        description: data.body.description,
        images: data.body.images,
        category: data.body.category,
        price: data.body.price,
        isActive: data.body.isActive,
    });

    return newProduct.save().then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Unable to create product at this time',
                },
            };
        }

        return {
            status: 201,
            data: {
                status: 201,
                message: `${result.name} product has been created`,
                product: result,
            },
        };
    });
};

// ----- [PD/RAA] Retrieve All Active ----- //
module.exports.retrieveAllActive = () => Product.find({ isActive: true }).sort({ name: 'asc' }).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Failed to retrieve products at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            status: 200,
            count: result.length,
            products: result,
        },
    };
});

// ----- [PD/RAABH] Retrieve All Active By Handle ----- //
module.exports.retrieveAllActiveByHandle = (data) => {
    if (data.params.collectionsHandle !== 'all') {
        return Product.find({ isActive: true, category: data.params.collectionsHandle }).sort({ name: 'asc' }).then((result, error) => {
            if (error) {
                return {
                    status: 400,
                    data: {
                        status: 400,
                        message: 'Failed to retrieve products at this time',
                    },
                };
            }

            return {
                status: 200,
                data: {
                    status: 200,
                    count: result.length,
                    products: result,
                },
            };
        });
    }

    return Product.find({ isActive: true }).sort({ name: 'asc' }).then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Failed to retrieve products at this time',
                },
            };
        }

        return {
            status: 200,
            data: {
                status: 200,
                count: result.length,
                products: result,
            },
        };
    });
};

// ----- [PD/RF] Retrieve Featured ----- //
module.exports.retrieveFeatured = () => Product.find({ isActive: true }).limit(6).sort({ name: 'desc' }).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Failed to retrieve products at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            status: 200,
            count: result.length,
            products: result,
        },
    };
});

// ----- [PD/RA] Retrieve All ----- //
module.exports.retrieveAll = () => Product.find({}).sort({ name: 'asc' }).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Failed to retrieve products at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            status: 200,
            count: result.length,
            products: result,
        },
    };
});

// ----- [PD/R] Retrieve ----- //
module.exports.retrieve = (data) => Product.findOne({ _id: data.params.productId }).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Unable to retrieve product at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            status: 200,
            product: result,
        },
    };
}).catch((error) => ({
    status: 404,
    data: {
        status: 404,
        message: `Product with this id:${error.value} cannot be found`,
    },
}));

// ----- [PD/RBH] Retrieve By Handle ----- //
// eslint-disable-next-line max-len
module.exports.retrieveByHandle = (data) => Product.findOne({ handle: data.params.productHandle }).then((result, error) => {
    if (error) {
        return {
            status: 400,
            data: {
                status: 400,
                message: 'Unable to retrieve product at this time',
            },
        };
    }

    return {
        status: 200,
        data: {
            status: 200,
            product: result,
        },
    };
}).catch((error) => ({
    status: 404,
    data: {
        status: 404,
        message: `Product with this id:${error.value} cannot be found`,
    },
}));

// ----- [PD/U] Update ----- //
module.exports.update = async (data) => {
    if (data.isAdmin === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    return Product.findByIdAndUpdate(data.params.productId, {
        name: data.body.name,
        handle: data.body.handle,
        description: data.body.description,
        price: data.body.price,
        images: data.body.images,
        category: data.body.category,
        isActive: data.body.isActive,
        updatedOn: new Date(),
    }, {
        returnDocument: 'after',
        runValidators: true,
    }).then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Unable to update product at this time',
                },
            };
        }

        return {
            status: 200,
            data: {
                status: 200,
                message: `${result.name} product has been updated`,
                product: result,
            },
        };
    });
};

// -------------------------------- //
// ----- [PD] Product Archive ----- //
// -------------------------------- //

// ----- [PA/SA] Set Active -----//
module.exports.setActive = async (data, newStatus) => {
    if (data.isAdmin === false) {
        return {
            status: 401,
            data: {
                status: 401,
                message: 'User is unauthorized to do this action',
            },
        };
    }

    return Product.findByIdAndUpdate(data.params.productId, {
        isActive: newStatus,
        updatedOn: new Date(),
    }, {
        returnDocument: 'after',
        runValidators: true,
    }).then((result, error) => {
        if (error) {
            return {
                status: 400,
                data: {
                    status: 400,
                    message: 'Unable to archive product at this time',
                },
            };
        }

        return {
            status: 200,
            data: {
                status: 200,
                message: `${result.name} product has been ${newStatus ? 'unarchived' : 'archived'}`,
                product: {
                    _id: result._id,
                    name: result.name,
                    handle: result.handle,
                    isActive: result.isActive,
                },
            },
        };
    }).catch((error) => ({
        status: 404,
        data: {
            status: 404,
            message: `Product with this id:${error.value} cannot be found`,
        },
    }));
};
