/* eslint-disable no-console */
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require('./routes/userRoutes.js');
const productRoutes = require('./routes/productRoutes.js');
const orderRoutes = require('./routes/orderRoutes.js');
const reviewRoutes = require('./routes/reviewRoutes.js');

const app = express();

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({
    extended: true,
}));

app.use('/user', userRoutes);
app.use('/product', productRoutes);
app.use('/order', orderRoutes);
app.use('/review', reviewRoutes);

mongoose.connect('mongodb+srv://root:toor@shop01.dsdirga.mongodb.net/shop01_backend?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
});
mongoose.connection.once('open', () => console.log('Now connected to Mongo DB Atlas.'));

const port = process.env.PORT || 4000;
app.listen(port, () => console.log(`API is now online on port ${port}`));


